// SPDX-FileCopyrightText: 2023 Mirko Adam <mirko@four-m.de
//
// SPDX-License-Identifier: AGPL-3.0-or-later

#include "main.h"


game_t* init(int argc, char **argv)
{
    initscr();
    refresh();
    nodelay(stdscr, TRUE);
    timeout(TIME_DELAY);
    keypad(stdscr, TRUE);
    noecho();

    game_t* game = malloc(sizeof(game_t));
    if(game == NULL)
    {
        errorControl("\nERROR: Could not allocate memory for game data\n");
        abort();
    }
    game->verbose = checkVerbose(argc, argv);
    if (game->verbose)
    {
        argc--;
    }
    verbosing("Initializing variables...", game->verbose);
    game->currentGameState = PREPARING;
    drawInitWindow(game);
    if(has_colors())
    {
        start_color();
        use_default_colors();
        init_color(COLOR_GREEN, 0, 1000, 0);
        init_color(COLOR_YELLOW, 1000, 1000, 0);
        init_color(COLOR_RED, 1000, 0, 0);
        init_color(COLOR_BLACK, 0, 0, 0);
        init_pair(2, COLOR_YELLOW, COLOR_YELLOW);
        init_pair(3, COLOR_RED, COLOR_RED);
        init_pair(4, COLOR_BLACK, COLOR_RED);
        init_pair(5, COLOR_GREEN, COLOR_BLACK);
        init_pair(6, COLOR_BLACK, COLOR_GREEN);
        curs_set(0);
    }
    else
    {
        emergencyCleanup(game, "Display does not support colors, game won't work");
    }
    verbosing("done!\n", game->verbose);
    return game;
}

mainMenuState mainMenu(game_t *game, mainMenuState state) {
    int key;
    drawMainMenu(game, state);

    while(true)
    {
        key = getch();
        switch(key)
        {
            case KEY_DOWN: state = (state + 1) % NUM_MAIN_MENU;
            break;
            case KEY_UP: state = (state - 1) % NUM_MAIN_MENU;
                break;
            case 'h': drawHelpWindow(game);
                break;
            case '\n': return state;
            case 'q': return QUIT;
            default: break;
        }
        drawMainMenu(game, state);
    }
}

void gameMenu(game_t *game) {
    int key;
    initGame(game);
    game->currentGameState = RUNNING;
    drawGameWindow(game);
    while(game->currentGameState == RUNNING)
    {
        key = getch();
        if(key != ERR)
        {
            inputHandler(game, key);
        }
        if(key == 'q' || key == 'Q')
        {
            return;
        }
        drawGameWindow(game);
    }
    drawGameEndWindow(game);
    while((key = getch()) != 'q' && key != 'Q')
    {
        if(key == 'h' || key == 'H')
        {
            key = getch();
            while(key != 'q' && key != 'Q' && key != 'h' && key != 'H')
            {
                key = getch();
            }
        }

    }
}

void inputHandler(game_t *game, int key) {
    switch (key) {
        case 'h' :
            helpMenu(game);
            break;
        case KEY_LEFT:
            leftField(game);
            break;
        case KEY_RIGHT:
            rightField(game);
            break;
        case KEY_UP:
            colorUp(game);
            break;
        case KEY_DOWN:
            colorDown(game);
            break;
        case '\n':
            checkRound(game);
            break;
        default:
            break;
    }
}

void helpMenu(game_t *game) {
    int key = 0;
    drawHelpWindow(game);
    while(key != 'q' && key != 'Q' && key != 'h' && key != 'H')
    {
        key = getch();
    }
}

void licenseMenu(game_t *game) {
    int key = 0;
    game->licenseLine = 0;
    game->maxLicenseLine = FALSE;
    drawLicenseWindow(game);
    while(true)
    {
        key = getch();
        switch(key) {
            case 'q':
                return;
            case 'Q':
                return;
            case 'h':
                return;
            case 'H':
                return;
            case KEY_UP:
                licenseUp(game);
                break;
            case KEY_DOWN:
                licenseDown(game);
                break;
            default:
                break;
        }
        drawLicenseWindow(game);
    }
}

int main(int argc, char **argv) {
    game_t *game = init(argc, argv);
    srand((unsigned int)time(NULL));
    mainMenuState menuState = START;
    while(menuState != QUIT)
    {
        menuState = mainMenu(game, menuState);
        switch(menuState)
        {
            case START: gameMenu(game);
            break;
            case HELP_MENU:
                helpMenu(game);
            break;
            case LICENSE:
                licenseMenu(game);
            break;
            default: break;
        }
        game->currentGameState = PREPARING;
    }
    cleanup(game);
    return EXIT_SUCCESS;
}
