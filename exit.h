// SPDX-FileCopyrightText: 2023 Mirko Adam <mirko@four-m.de
//
// SPDX-License-Identifier: AGPL-3.0-or-later

#ifndef SUPERBRAINCONSOLE_EXIT_H
#define SUPERBRAINCONSOLE_EXIT_H

#include <stdlib.h>
#include <stdio.h>
#include "game.h"

void cleanup(game_t *game);

void emergencyCleanup(game_t *game, char *errorString);

void deleteWindows(game_t *game);

#endif
