// SPDX-FileCopyrightText: 2023 Mirko Adam <mirko@four-m.de
//
// SPDX-License-Identifier: AGPL-3.0-or-later

#include "draw.h"

static WINDOW *initWindow(int pos_y, int pos_x, int size_y, int size_x, game_t *game)
{
    WINDOW *window = newwin(pos_y, pos_x, size_y, size_x);
    if(!window)
    {
        emergencyCleanup(game, "Creating Ncurses window failed");
    }
    return window;
}

void drawInitWindow(game_t *game)
{
    game->startWindow = initWindow(MAIN_WIN_Y + 2, MAIN_WIN_X + 2, 0, 0, game);
    game->gameWindow = initWindow(MAIN_WIN_Y + 2, MAIN_WIN_X + 2, 0, 0, game);
    game->helpWindow = initWindow(SUB_WIN_Y, SUB_WIN_X, (MAIN_WIN_Y - SUB_WIN_Y) / 2, (MAIN_WIN_X - SUB_WIN_X) / 2, game);
    game->licenseWindow = initWindow(SUB_WIN_Y, SUB_WIN_X, (MAIN_WIN_Y - SUB_WIN_Y) / 2, (MAIN_WIN_X - SUB_WIN_X) / 2, game);
}

void drawMainMenu(game_t *game, mainMenuState menuState)
{
    char menu[NUM_MAIN_MENU][16] = {"New Game", "Help", "License", "Quit"};
    wclear(game->startWindow);
    wattron(game->startWindow, COLOR_PAIR(5));
    box(game->startWindow, 0, 0);
    mvwprintw(game->startWindow, 0, MAIN_WIN_X / 2 - 7, "[ Main Menu ]");
    for (int i = 0; i < NUM_MAIN_MENU; i++)
    {
        if(i == (int) menuState)
        {
            mvwaddstr(game->startWindow, MAIN_WIN_Y / 3 + i, MAIN_WIN_Y / 6 - 3, "-> ");
            wattron(game->startWindow, A_REVERSE);
        }
        mvwaddstr(game->startWindow, MAIN_WIN_Y / 3 + i, MAIN_WIN_X / 6, menu[i]);
        wattroff(game->startWindow, A_REVERSE);
    }
    mvwaddstr(game->startWindow, 2 * MAIN_WIN_Y / 3, MAIN_WIN_X / 6, "Use arrow keys to move! Press ENTER to select");
    wrefresh(game->startWindow);
}

void drawHints(game_t *game, int yPos, int xPos, int round)
{
    for(int i = 0; i < FIELDS; i++)
    {
        switch (game->playerHints[round][i]) {
            case CORRECT_COLOR:
                wattron(game->gameWindow, COLOR_PAIR(2));
                break;
            case CORRECT_POSITION:
                wattron(game->gameWindow, COLOR_PAIR(3));
                break;
            default:
                break;
        }
        mvwprintw(game->gameWindow, yPos + 1, xPos + i, COLORED_RECTANGLE);
        wattroff(game->gameWindow, COLOR_PAIR(2));
        wattroff(game->gameWindow, COLOR_PAIR(3));
    }
}

void drawGuesses(game_t *game, int yPos, int xPos, int round, bool collisions, bool guess)
{
    for(int i = 0; i < FIELDS; i++) {
        if(collisions)
        {
            if(game->clashes[i] == TRUE)
            {
                wattron(game->gameWindow, COLOR_PAIR(4));
            }
            if(game->currentField == i)
            {
                wattroff(game->gameWindow, COLOR_PAIR(4));
                wattron(game->gameWindow, COLOR_PAIR(6));
            }
        }
        if(guess)
        {
            mvwprintw(game->gameWindow, yPos + 1, xPos + i, "%d", game->toGuess[i]);
        }
        else
        {
            mvwprintw(game->gameWindow, yPos + 1, xPos + i, "%d", game->playerTryArea[round][i]);
        }
        wattroff(game->gameWindow, COLOR_PAIR(4));
        wattroff(game->gameWindow, COLOR_PAIR(6));
        wattron(game->gameWindow, COLOR_PAIR(5));
    }
}

void drawPatterns(game_t *game, int yPos, int xPos, int round, bool hints, bool collisions, bool guess)
{
    wattron(game->gameWindow, COLOR_PAIR(5));
    mvwprintw(game->gameWindow, yPos + 1, xPos - 1, "|");
    mvwprintw(game->gameWindow, yPos + 1, xPos + FIELDS, "|");
    for(int i = 0; i < FIELDS; i++)
    {
        mvwprintw(game->gameWindow, yPos, xPos + i, "-");
        mvwprintw(game->gameWindow, yPos + 2, xPos + i, "-");
    }
    if(hints)
    {
        drawHints(game, yPos, xPos, round);
    }
    else
    {
        drawGuesses(game, yPos, xPos, round, collisions, guess);
    }
}

void drawGameWindow(game_t *game) {
    wclear(game->gameWindow);
    wattron(game->gameWindow, COLOR_PAIR(5));
    box(game->gameWindow, 0, 0);
    mvwprintw(game->gameWindow, 0, MAIN_WIN_X / 2 - 9, "[ Superbrain Console ]");

    int gameSpaceX = MAIN_WIN_X / 2 - (FIELDS + (DISTANCE_HINTS_FIELD / 2));
    int gameSpaceY = MAIN_WIN_Y / 2 - 1;

    mvwprintw(game->gameWindow, gameSpaceY - 1, gameSpaceX - 1, "Hints");
    mvwprintw(game->gameWindow, gameSpaceY - 1, gameSpaceX + FIELDS + DISTANCE_HINTS_FIELD - 3, "Game Space");

    gameSpaceY = gameSpaceY + 3 * (ROUNDS - 1);
    for(int y = 0; y < ROUNDS; y++)
    {
        int yPos = y * 3;
        drawPatterns(game, gameSpaceY - yPos, gameSpaceX, y, TRUE, FALSE, 0);
        drawPatterns(game, gameSpaceY - yPos, gameSpaceX + FIELDS + DISTANCE_HINTS_FIELD, y, FALSE, FALSE, FALSE);
        if(game->currentRound == y) {
            drawPatterns(game, gameSpaceY - yPos, gameSpaceX + FIELDS + DISTANCE_HINTS_FIELD, y, FALSE, TRUE, FALSE);
        }
        else
        {
            drawPatterns(game, gameSpaceY - yPos, gameSpaceX + FIELDS + DISTANCE_HINTS_FIELD, y, FALSE, FALSE, FALSE);
        }
    }
    wrefresh(game->gameWindow);
}

void drawGameEndWindow(game_t *game) {
    drawGameWindow(game);

    int solutionSpaceX = MAIN_WIN_X / 2 - 2;
    int solutionSpaceY = MAIN_WIN_Y / 2 - 14;

    mvwprintw(game->gameWindow, solutionSpaceY, solutionSpaceX - 22, "You");
    if(game->currentGameState == WON)
    {
        mvwprintw(game->gameWindow, solutionSpaceY, solutionSpaceX - 18, "WON! Congratulations!");
    }
    if(game->currentGameState == OVER)
    {
        mvwprintw(game->gameWindow, solutionSpaceY, solutionSpaceX - 18, "LOST! But remember: The greatest teacher failure is!");
    }
    mvwprintw(game->gameWindow, solutionSpaceY + 2, solutionSpaceX - 13, "The pattern to be guessed was:");
    drawPatterns(game, solutionSpaceY + 4, solutionSpaceX, 0, FALSE, FALSE, TRUE);
    mvwprintw(game->gameWindow, solutionSpaceY + 7, solutionSpaceX - 16, "For comparison, your last guess was:");
    drawPatterns(game, solutionSpaceY + 8, solutionSpaceX, game->currentRound - 1, FALSE, FALSE, FALSE);
    wrefresh(game->gameWindow);
}

void drawHelpWindow(game_t *game)
{
    wclear(game->helpWindow);
    wattron(game->helpWindow, COLOR_PAIR(5));
    box(game->helpWindow, 0, 0);
    mvwprintw(game->helpWindow, 0, SUB_WIN_X / 2 - 4, "[ Help ]");

    mvwprintw(game->helpWindow, SUB_WIN_Y / 6, SUB_WIN_X / 7, "h        -> Help Window");
    mvwprintw(game->helpWindow, SUB_WIN_Y / 6 + 1, SUB_WIN_X / 7, "q        -> Quit");
    mvwprintw(game->helpWindow, SUB_WIN_Y / 6 + 2, SUB_WIN_X / 7, "KEY UP   -> Change Number at current position (+1)");
    mvwprintw(game->helpWindow, SUB_WIN_Y / 6 + 3, SUB_WIN_X / 7, "KEY DOWN -> Change Number at current position (-1)");
    mvwprintw(game->helpWindow, SUB_WIN_Y / 6 + 4, SUB_WIN_X / 7, "KEY DOWN -> Change Number at current position (-1)");
    mvwprintw(game->helpWindow, SUB_WIN_Y / 6 + 5, SUB_WIN_X / 7, "KEY LEFT -> Go Left");
    mvwprintw(game->helpWindow, SUB_WIN_Y / 6 + 6, SUB_WIN_X / 7, "KEY LEFT -> Go Right");
    mvwprintw(game->helpWindow, SUB_WIN_Y / 6 + 7, SUB_WIN_X / 7, "ENTER    -> Check Guess");
    mvwprintw(game->helpWindow, 3 * SUB_WIN_Y / 4, SUB_WIN_X / 7, "Press q or h to exit help screen!");

    wrefresh(game->helpWindow);
}

void drawLicenseWindow(game_t *game) {
    wclear(game->licenseWindow);
    wattron(game->licenseWindow, COLOR_PAIR(5));
    box(game->licenseWindow, 0, 0);
    mvwprintw(game->licenseWindow, 0, SUB_WIN_X / 2 - 5, "[ License ]");

    int y = 2 - game->licenseLine;
    int maxY = SUB_WIN_Y - 1;
    int x = 2;
    int minX = x;
    int maxX = SUB_WIN_X - 2;

    FILE *license = fopen("../LICENSE", "r");

    if (license == NULL) {
        emergencyCleanup(game, "Could not open license file");
    }

    if(game->maxLicenseLine)
    {
        return;
    }

    int currentChar;
    while((currentChar = fgetc(license)) != EOF && y != maxY)
    {
        if (currentChar != '\n')
        {
            if(y >= 2)
            {
                mvwprintw(game->licenseWindow, y, x, "%c", currentChar);
            }
            x++;
        }
        if(currentChar == '\n' || x == maxX)
        {
            y++;
            x = minX;
        }
    }
    game->maxLicenseLine = y < 2;

    fclose(license);
    wrefresh(game->licenseWindow);
}
