// SPDX-FileCopyrightText: 2023 Mirko Adam <mirko@four-m.de
//
// SPDX-License-Identifier: AGPL-3.0-or-later

#include "game.h"

void createToGuess(game_t *game)
{
    for(int i = 0; i < FIELDS; i++)
    {
        game->toGuess[i] = (char) (rand() % COLORS);
        for(int j = 0; j < i; j++)
        {
            while(game->toGuess[i] == game->toGuess[j])
            {
                game->toGuess[i] = (char) (rand() % COLORS);
            }
        }
    }
}

void resetUserState(game_t *game) {
    game->currentField = 0;
    game->currentRound = 0;
    for(int i = 0; i < ROUNDS; i++)
    {
        for(int j = 0; j < FIELDS; j++)
        {
            game->playerTryArea[i][j] = 0;
        }
        for(int j = 0; j < FIELDS; j++)
        {
            game->playerHints[i][j] = NOTHING;
        }
    }
    for(int i = 0; i < FIELDS; i++)
    {
        game->clashes[i] = FALSE;
    }
}

void initGame(game_t *game)
{
    game->check = 0;
    createToGuess(game);
    resetUserState(game);
}

void leftField(game_t *game)
{
    if(game->currentField > 0)
    {
        game->currentField--;
    }
}

void rightField(game_t *game)
{
    if(game->currentField < FIELDS - 1)
    {
        game->currentField++;
    }
}

void resetClashes(game_t *game)
{
    for(int i = 0; i < FIELDS; i++)
    {
        game->clashes[i] = FALSE;
    }
}

void colorUp(game_t *game)
{
    if(game->playerTryArea[game->currentRound][game->currentField] < COLORS - 1)
    {
        game->playerTryArea[game->currentRound][game->currentField]++;
    }
    if(game->clashes[game->currentField])
    {
        resetClashes(game);
    }
}

void colorDown(game_t *game)
{
    if(game->playerTryArea[game->currentRound][game->currentField] > 0)
    {
        game->playerTryArea[game->currentRound][game->currentField]--;
    }
    if(game->clashes[game->currentField])
    {
        resetClashes(game);
    }
}

bool checkClashes(game_t *game) {
    for(int i = 0; i < FIELDS; i++)
    {
        if(game->clashes[i] == TRUE)
        {
            return TRUE;
        }
    }
    bool trigger = FALSE;
    for(int i = 0; i < FIELDS; i++)
    {
        for(int j = 0; j < FIELDS; j++)
        {
            if(i != j && game->playerTryArea[game->currentRound][i] == game->playerTryArea[game->currentRound][j])
            {
                game->clashes[i] = TRUE;
                trigger = TRUE;
            }
        }
    }
    return trigger;
}

int checkRightColors(game_t *game) {
    int rightColors = 0;
    for(int i = 0; i < FIELDS; i++)
    {
        for(int j = 0; j < FIELDS; j++)
        {
            if(game->toGuess[i] == game->playerTryArea[game->currentRound][j])
            {
                rightColors++;
                break;
            }
        }
    }
    return rightColors;
}

int checkRightPositions(game_t *game) {
    int rightPositions = 0;
    for(int i = 0; i < FIELDS; i++)
    {
        if(game->toGuess[i] == game->playerTryArea[game->currentRound][i])
        {
            rightPositions++;
        }
    }
    return rightPositions;
}

void colorize(game_t *game, int cols, guessState state) {
    for(int i = 0; i < cols; i++)
    {
        game->playerHints[game->currentRound][i] = state;
    }
}

void checkRound(game_t *game)
{
    if (!checkClashes(game))
    {
        int numCols = checkRightColors(game);
        colorize(game, numCols, CORRECT_COLOR);
        numCols = checkRightPositions(game);
        colorize(game, numCols, CORRECT_POSITION);
        if(numCols == FIELDS)
        {
            game->currentGameState = WON;
        }
        game->currentRound++;
        if(game->currentRound == ROUNDS)
        {
            game->currentGameState = OVER;
        }
        game->currentField = 0;
    }
}

void licenseDown(game_t *game) {
    if(!game->maxLicenseLine)
    {
        game->licenseLine++;
    }
}

void licenseUp(game_t *game) {
    if(game->licenseLine >= 0)
    {
        game->licenseLine--;
        game->maxLicenseLine = FALSE;
    }
}
