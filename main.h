// SPDX-FileCopyrightText: 2023 Mirko Adam <mirko@four-m.de
//
// SPDX-License-Identifier: AGPL-3.0-or-later

#ifndef SUPERBRAINCONSOLE_MAIN_H
#define SUPERBRAINCONSOLE_MAIN_H

// the includes

#include <stdlib.h>
#include <stdio.h>
#include <ncurses.h>
#include <time.h>
#include "game.h"
#include "draw.h"
#include "exit.h"

// the functions

game_t* init(int argc, char **argv);

mainMenuState mainMenu(game_t *game, mainMenuState state);

void gameMenu(game_t *game);

void helpMenu(game_t *game);

void licenseMenu(game_t *game);

void inputHandler(game_t *game, int key);

#endif
