// SPDX-FileCopyrightText: 2023 Mirko Adam <mirko@four-m.de
//
// SPDX-License-Identifier: AGPL-3.0-or-later

#include "exit.h"

void deleteWindows(game_t *game)
{
    delwin(game->gameWindow);
    delwin(game->licenseWindow);
    delwin(game->helpWindow);
    delwin(game->startWindow);
    endwin();
}

void cleanup(game_t* game)
{
    deleteWindows(game);
    free(game);
}

void emergencyCleanup(game_t* game, char *errorString)
{
    cleanup(game);
    severeErrorControl(errorString);
    abort();
}
