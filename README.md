<!--
SPDX-FileCopyrightText: 2023 Mirko Adam <mirko@four-m.de

SPDX-License-Identifier: GFDL-1.3-or-later
-->

# Superbrain Console

---

[![GPL enforced badge](https://img.shields.io/badge/GPL-enforced-blue.svg "This project enforces the GPL.")](https://gplenforced.org)
![Woodpecker badge](https://woodpecker.four-m.de/api/badges/Elshid/superbrain-console/status.svg "Status of the building process")

The game Superbrain Console is an implementation of the game "Superbrain" (or "Superhirn" in German)
in C that can be played directly in the console.


## Usage

`./superbrain`

## Dependencies

* CMake
* GCC
* Ncurses

## License

This program is Free Software! Feel Free to run, modify and share under the conditions of the AGPL!
