// SPDX-FileCopyrightText: 2023 Mirko Adam <mirko@four-m.de
//
// SPDX-License-Identifier: AGPL-3.0-or-later

#ifndef SUPERBRAINCONSOLE_DRAW_H
#define SUPERBRAINCONSOLE_DRAW_H

#include <stdlib.h>
#include <stdio.h>
#include <ncurses.h>
#include "game.h"
#include "exit.h"

#define NUM_MAIN_MENU 4

#define MAIN_WIN_X 100
#define MAIN_WIN_Y 32

#define SUB_WIN_X 70
#define SUB_WIN_Y 20

#define COLORED_RECTANGLE " "
#define DISTANCE_HINTS_FIELD 10

void drawInitWindow(game_t *game);

void drawMainMenu(game_t *game, mainMenuState menuState);

void drawHelpWindow(game_t *game);

void drawGameWindow(game_t *game);

void drawLicenseWindow(game_t *game);

void drawGameEndWindow(game_t *game);

void drawHints(game_t *game, int yPos, int xPos, int round);

void drawGuesses(game_t *game, int yPos, int xPos, int round, bool collisions, bool guess);

void drawPatterns(game_t *game, int yPos, int xPos, int round, bool hints, bool collisions, bool guess);

#endif
