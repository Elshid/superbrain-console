// SPDX-FileCopyrightText: 2023 Mirko Adam <mirko@four-m.de
//
// SPDX-License-Identifier: AGPL-3.0-or-later

#ifndef SUPERBRAINCONSOLE
#define SUPERBRAINCONSOLE

// the includes

#include <stdlib.h>
#include <stdio.h>
#include <ncurses.h>
#include <stdbool.h>
#include "debug.h"

// the defines

#define TIME_DELAY 50
#define ROUNDS 6
#define COLORS 6
#define FIELDS 4

// the typedefs

typedef enum {
    NOTHING, CORRECT_COLOR, CORRECT_POSITION
} guessState;

typedef enum {
    START, HELP_MENU, LICENSE, QUIT
} mainMenuState;

typedef enum {
    PREPARING, RUNNING, WON, OVER
}gameState;

struct gameData
{
    char playerTryArea[ROUNDS][FIELDS];
    bool clashes[FIELDS];
    guessState playerHints[ROUNDS][FIELDS];
    char toGuess[FIELDS];
    bool verbose;
    int check;
    int currentRound;
    int currentField;
    gameState currentGameState;
    int licenseLine;
    bool maxLicenseLine;

    WINDOW *startWindow;
    WINDOW *helpWindow;
    WINDOW *gameWindow;
    WINDOW *licenseWindow;
};

typedef struct gameData game_t;

// the functions

void initGame(game_t *game);

void resetUserState(game_t *pData);

void createToGuess(game_t *game);

void leftField(game_t *game);

void rightField(game_t *game);

void colorUp(game_t *game);

void colorDown(game_t *game);

void checkRound(game_t *game);

void resetClashes(game_t *game);

bool checkClashes(game_t *game);

int checkRightColors(game_t *game);

int checkRightPositions(game_t *game);

void colorize(game_t *game, int cols, guessState state);

void licenseUp(game_t *game);

void licenseDown(game_t *game);

#endif
